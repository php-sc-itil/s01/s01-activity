<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 01: PHP Basics and Selection Control Structures</title>
</head>
<body>
	<h1>Letter-Based Grading</h1>

	<p><?php echo getLetterGrade (87); ?></p>
</body>
</html>