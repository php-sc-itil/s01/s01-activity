<?php

function getLetterGrade ($grade) {
	if($grade <= 98 && >=100) {
		return $grade . 'is equivalent to A+';
	}
	else if($grade <=95 && >= 97) {
		return $grade . 'is equivalent to A';
	}
	else if($grade <=92 && >=94) {
		return $grade . 'is equivalent to A-';
	}
	else if($grade <=89 && >=91) {
		return  $grade . 'is equivalent to B+';
	}
	else if($grade <=86 && >=88) {
		return $grade . 'is equivalent to B';
	}
	else if($grade <=83 && >=85) {
		return $grade . 'is equivalent to B-';
	}
	else if($grade <=80 && >=82) {
		return $grade . 'is equivalent to C+';
	}
	else if($grade <=77 && >=79) {
		return $grade . 'is equivalent to C';
	}
	else if($grade <=75 && >=76) {
		return $grade .'is equivalent to C-';
	}
	else {
		return $grade . 'is equivalent to D';
	}
}

?>